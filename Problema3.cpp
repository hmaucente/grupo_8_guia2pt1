#include <iostream>
#include <cstdlib>
#include <ctime>

#define SIZE 256
#define BITS 8

using namespace std;

void binaryPrintInt(int n);


int main() {
	
	// arreglo aleatorio de 256 enteros entre 0 y 255 c/u
	int arr[SIZE];
	srand(time(0)); 
	                   
	for(int i = 0; i < SIZE; i++) {
		arr[i] = rand() % SIZE;
		cout << "numero aleatorio #" << i + 1 << ": " << arr[i] << endl;
	}
	
	
	// imprimir a pantalla representacion binaria de entero
	int num = rand();
	cout << "La representacion binaria del entero " << num << " es: ";
	binaryPrintInt(num);
	
	return 0;
}

void binaryPrintInt(int n) {
	for (int i = sizeof(int) * 8 - 1; i >= 0; i--) {
		bool bit = n & (1 << i);
		cout << bit;
	}
	cout << endl;
}

