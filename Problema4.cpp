#include <iostream>
#include <cstdlib>
#include <ctime>

#define MEDICIONES 24
#define TMIN -10
#define TMAX 45
#define DIV 100  // cuantos decimales, potencias de 10, 100 => hasta la centena, 1000 => hasta la milesima, etc

using namespace std;

typedef struct medicion {
	double temp;
	char time[5];
} medicion;

double medAvg(medicion *arr, int size);
medicion medMax(medicion *arr, int size);
medicion medMin(medicion *arr, int size);

int main() {
	
	srand(time(0));
	medicion meds[MEDICIONES];
	
	for(int i = 0; i < MEDICIONES; i++) {
		meds[i].temp = ((double)(rand() % DIV *( TMAX + 1 - TMIN))) / DIV + TMIN;
		sprintf(meds[i].time, "%02d:00", i%24); // se asume una medicion por hora comenzando a las 00:00, i%24 permite tener mas de 1 dia
		cout << "TEM" << i + 1 << ": " << meds[i].temp << " grados Celsius" << ", a las " << meds[i].time << " Hrs" << endl;
	}	
	
	cout << "Temperatura promedio: " << medAvg(meds, MEDICIONES) << " grados Celsius" << endl;
	medicion max = medMax(meds, sizeof(meds)/sizeof(medicion));
	medicion min = medMin(meds, sizeof(meds)/sizeof(medicion));
	cout << "Temperatura maxima: " << max.temp << " grados Celsius, a las " << max.time << " hrs" <<  endl;
	cout << "Temperatura minima: " << min.temp << " grados Celsius, a las " << min.time << " hrs" <<  endl;
	
	return 0;
}

double medAvg(medicion *arr, int size) {
	double sum = 0;	
	for (int i = 0; i < size; i++) sum += arr[i].temp;
	return sum / size;
}
medicion medMax(medicion *arr, int size) {
	medicion max = *arr;
	for (int i = 1; i < size; i++) if (arr[i].temp > arr[i - 1].temp) max = arr[i];
	return max;
}
medicion medMin(medicion *arr, int size) {
	medicion min = *arr;
	for (int i = 1; i < size; i++) if (arr[i].temp < arr[i - 1].temp) min = arr[i];
	return min;
}

